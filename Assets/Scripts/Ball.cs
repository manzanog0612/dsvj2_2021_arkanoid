﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    float speed = 10f;
    bool disabled = true;

    float minYPos = 1;

    public delegate void OnDisabledDelagate(GameObject ball);
    public OnDisabledDelagate onDisabled;

    public delegate void OnFellDelegate();
    public OnFellDelegate onFell;

    public delegate void OnHitBrickDelegate(GameObject brick);
    public OnHitBrickDelegate onHitBrick;

    public delegate void OnSpawnDelegate();
    public OnSpawnDelegate onSpawn;

    Vector3 dir = Vector3.up;


    public void DisableBall()
    {
        disabled = true;
    }

    void SetMovement()
    {
        transform.position += dir * speed;
        
    }

    void SetReboundAgainstPlayer(Transform player)
    {
        float diffBallToPlayerCenter = transform.position.x - player.position.x;

        float barModifier = 2 * diffBallToPlayerCenter / player.localScale.x;

        if (barModifier > 1) barModifier = 1;
        else if (barModifier < -1) barModifier = -1;

        if (Physics.Raycast(transform.position, Vector3.up, transform.localScale.x / 2 + 0.1f) ||
            Physics.Raycast(transform.position, Vector3.down, transform.localScale.x / 2 + 0.1f))
        {
            dir.x += barModifier;
        }

        dir.Normalize();
    }

    void ResetDir()
    {
        dir = Vector3.up;
    }

    void Start()
    {
        onFell += ResetDir;
        onSpawn?.Invoke();

    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Space) && disabled)
        {            
            disabled = false;
            dir = new Vector3(Random.Range(-1f, 1f), 1f, 0f).normalized; 
            GetComponent<Rigidbody>().velocity = dir * speed;
        }
        
        if (disabled)
        {
            onDisabled?.Invoke(gameObject);
            return;
        }

        if (transform.position.y - transform.localScale.y / 2 <= minYPos)
        { 
            onFell?.Invoke();
            return;
        }        
    }

    void FixedUpdate()
    {
        //SetMovement();
        GetComponent<Rigidbody>().velocity = dir * speed * Time.deltaTime * 50f;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            foreach (ContactPoint contact in collision.contacts)
            {
                dir = Vector3.Reflect(dir, contact.normal);
                return;
            }

            SetReboundAgainstPlayer(collision.transform);
        }
        else
        {
            if (collision.gameObject.CompareTag("Brick"))
            {
                onHitBrick?.Invoke(collision.gameObject);
            }
            foreach (ContactPoint contact in collision.contacts)
            {
                dir = Vector3.Reflect(dir, contact.normal);
                return;
            }
        }
    }

    void OnDestroy()
    {
        onDisabled = null;
        onFell = null;
    }
}
