﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class EndSceneUI : MonoBehaviour
{
    [SerializeField] Text dataText;
    [SerializeField] TextMeshProUGUI resultText;

    string FormatTime(float fTime)
    {
        TimeSpan t = TimeSpan.FromSeconds(fTime);

        return string.Format("{1:D2}:{2:D2}:{3:D2}", t.Hours, t.Minutes, t.Seconds, t.Milliseconds);
    }

    private void Awake()
    {
        dataText.text = "Lives\n" + GameplayData.instance.lives +
                        "\nScore\n" + GameplayData.instance.score +
                        "\nHighscore\n" + GameplayData.instance.highscore;

        if (GameplayData.instance.win)
            resultText.text = "You've Won!";
        else
            resultText.text = "You've Lost...";
    }
}