﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    Player player;
    Ball ball;
    BricksManager bricksManager;

    void ResetBall()
    {
        float ballPlayerDistance = 0.2f;
        Vector3 ballPos = transform.position;

        ballPos.y += player.transform.localScale.y / 2 + ball.transform.localScale.y / 2 + ballPlayerDistance;

        ball.transform.position = ballPos;

        ball.DisableBall();
    }

    void UpdateGameplayData()
    {
        GameplayData gd = GameplayData.instance;

        gd.lives = player.GetLives();
        gd.score = player.GetScore();
        //gd.time = Time.realtimeSinceStartup - timeInMenu;
    }

    void Awake()
    {
        player = GameObject.FindWithTag("Player").GetComponent<Player>();
        ball = GameObject.FindWithTag("Ball").GetComponent<Ball>();
        bricksManager = GameObject.FindWithTag("Bricks").GetComponent<BricksManager>();
    }

    void Start()
    {
        ball.onDisabled += player.FollowPlayer;
        ball.onFell += player.Die;
        ball.onFell += ResetBall;
        ball.onSpawn += ResetBall;
        ball.onHitBrick += player.AddScore;
        ball.onHitBrick += bricksManager.DestroyBrick;

        bricksManager.OnAllBricksDestroyed += GameManager.GetInstance().WinGame;

        player.onDie += GameManager.GetInstance().LoseGame;

    }

    void Update()
    {
        UpdateGameplayData();
    }

    void OnDestroy()
    {
        player.onDie = null;

        GameplayData.instance.lives = player.GetLives();
        GameplayData.instance.score = player.GetScore();
    }
}
