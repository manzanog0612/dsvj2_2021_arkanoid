﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] GameObject ballPrefab;

    public delegate void onDieDelegate();
    public onDieDelegate onDie;

    float speed = 20;
    int lives = 3;
    int score = 0;

    Vector3 initPosition;

    public void AddScore(GameObject brick)
    {
        score += 5;
    }

    public int GetScore()
    {
        return score;
    }

    public int GetLives()
    {
        return lives;
    }

    public void Die()
    {
        lives--;

        if (lives > 0)
        { 
            transform.position = initPosition;
        }
        else
        {
            onDie?.Invoke();
        }
    }

    void SetMovement()
    {
        float inputX = Input.GetAxis("Horizontal");

        Vector3 pos = transform.position;
        pos += Vector3.right * inputX * speed * Time.deltaTime;

        if (PositionAllowed(pos))
            transform.position = pos;
    }

    bool PositionAllowed(Vector3 pos)
    {
        BricksManager bi = GameObject.FindWithTag("Bricks").GetComponent<BricksManager>();

        Vector3 initPos = bi.GetInitialPos();
        Vector3 finalPos = bi.GetFinalPos();

        return pos.x - transform.localScale.x / 2 > initPos.x && pos.x + transform.localScale.x / 2 < finalPos.x;
    }

    public void FollowPlayer(GameObject ball)
    {
        float ballPlayerDistance = 0.2f;
        Vector3 ballPos = transform.position;

        ballPos.y += transform.localScale.y / 2 + ballPrefab.transform.localScale.y / 2 + ballPlayerDistance;

        ball.transform.position = ballPos;
    }

    void Awake()
    {
        initPosition = transform.position;
    }

    void Update()
    {
        SetMovement();
    }
}
