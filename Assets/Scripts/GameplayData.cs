﻿using System.Collections;
using UnityEngine;

public class GameplayData
{
    public static GameplayData instance;

    public string highScore_S = "HighScore";

    public GameplayData()
    {
        highscore = PlayerPrefs.GetFloat(highScore_S, 0);
    }

    public void SetHighscore()
    {
        if (highscore < score)
            highscore = score;
    }

    public int lives = 0;
    public int score = 0;
    public float time = 0;
    public float highscore;

    public bool win = false;
}

