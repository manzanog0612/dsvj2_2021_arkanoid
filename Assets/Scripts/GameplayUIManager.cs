﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class GameplayUIManager : MonoBehaviour
{
    [SerializeField] Text lives;
    [SerializeField] Text score;
    [SerializeField] Text highscore;

    GameplayData gd;

    void Start()
    {
        gd = GameplayData.instance;
    }

    void Update()
    {
        lives.text = "Lives: " + gd.lives.ToString();
        score.text = "Score: " + gd.score.ToString();
        highscore.text = "Highscore\n" + gd.highscore.ToString();
    }
}
