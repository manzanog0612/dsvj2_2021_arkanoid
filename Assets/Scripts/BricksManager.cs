﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BricksManager : MonoBehaviour
{
    [SerializeField] GameObject brickPrefab;

    List<GameObject> bricks;

    public delegate void OnAllBricksDestroyedDelegate();
    public OnAllBricksDestroyedDelegate OnAllBricksDestroyed;

    short rows = 5;
    short columns = 9;

    float bWidth = 2;
    float bHeight = 1;

    Vector3 initPos;
    Vector3 finalPos;

    enum colors
    {
        red, blue, green, yellow, magenta, last
    }

    Color GetColor(colors c)
    {
        switch (c)
        {
            case colors.red:
                return Color.red;
            case colors.blue:
                return Color.blue;
            case colors.green:
                return Color.green;
            case colors.yellow:
                return Color.yellow;
            case colors.magenta:
                return Color.magenta;
            default:
                return Color.magenta;
        }
    }

    void SetBrickColor(GameObject brick)
    {
        colors aux = (colors)Random.Range(0, (int)colors.last);
        brick.GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", GetColor(aux) * 20);
        brick.GetComponentInChildren<MeshRenderer>().material.color = Color.black;
    }

    public Vector3 GetInitialPos()
    {
        return initPos;
    }

    public Vector3 GetFinalPos()
    {
        return finalPos;
    }

    public bool AnyBrickLeft()
    {
        return bricks.Count > 0;
    }

    void Awake()
    {
        initPos = new Vector3(-9, 16, 1);
        finalPos = new Vector3(9, 16, 1);

        bricks = new List<GameObject>();

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                Vector3 pos = new Vector3(initPos.x + bWidth / 2f + j * bWidth, initPos.y - bHeight / 2f - i * bHeight, initPos.z);

                GameObject brick = Instantiate(brickPrefab, pos, Quaternion.identity, transform);

                SetBrickColor(brick);

                bricks.Add(brick);
            }
        }
    }

    public void DestroyBrick(GameObject brick)
    {
        bricks.Remove(brick);
        Destroy(brick);

        if (!AnyBrickLeft())
            OnAllBricksDestroyed?.Invoke();
    }
}
