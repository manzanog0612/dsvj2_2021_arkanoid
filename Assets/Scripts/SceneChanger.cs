﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{
    public void GoToGame()
    {
        GameManager.GetInstance().ChangeScene(GameManager.scenes.game);        
    }

    public void GoToEndScene()
    {
        GameManager.GetInstance().ChangeScene(GameManager.scenes.endScene);        
    }

    public void GoToMenu()
    {
        GameManager.GetInstance().ChangeScene(GameManager.scenes.menu);        
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
