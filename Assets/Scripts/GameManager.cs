﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    

    public static GameManager GetInstance()
    {
        return instance;
    }

    public enum scenes
    {
        menu,
        game,
        endScene
    }

    public void ChangeScene(scenes s)
    {
        switch (s)
        {
            case scenes.menu:
                GoToMenu();
                break;
            case scenes.game:
                GoToGame();
                break;
            case scenes.endScene:
                GoToEndScene();
                break;
            default:
                break;
        }
    }

    public void LoseGame()
    {
        GameplayData.instance.win = false;
        GoToEndScene();
    }

    public void WinGame()
    {
        GameplayData.instance.win = true;
        GoToEndScene();
    }

    public void GoToGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void GoToEndScene()
    {
        GameplayData.instance.SetHighscore();
        PlayerPrefs.SetFloat(GameplayData.instance.highScore_S, GameplayData.instance.highscore);
        SceneManager.LoadScene("FinalScene");
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            Debug.Log(name + " has been destroyed, there's already one Game Manager created.");
            return;
        }
        instance = this;
        Debug.Log(name + " has been created and is now the Game Manager.");
        DontDestroyOnLoad(gameObject);

        GameplayData.instance = new GameplayData();
    }
}
